FROM php:7.1-apache

RUN apt-get update \
  && apt-get install -y \
    git \
    zlib1g-dev \
    libicu-dev \
    libmcrypt-dev \
    pkg-config \
    libssl-dev \
    libxml2-dev \
    g++ \
 && docker-php-ext-install intl mbstring pdo_mysql zip mcrypt gettext soap \
 && pecl install xdebug \
 && a2enmod rewrite \
 && a2enmod headers \
 && apt-get clean \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && mv /var/www/html /var/www/public \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www