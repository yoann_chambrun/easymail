<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

/**
 * List of enabled modules for this betting.
 *
 * This should be an array of module namespaces used in the betting.
 */
return [
    'Zend\Form',
    'Zend\I18n',
    'Zend\Mvc\Console',
    'Zend\Mvc\I18n',
    'Zend\Mvc\Plugin\FlashMessenger',
    'Zend\Mvc\Plugin\Prg',
    'Zend\Router',
    'Zend\Validator',
    'Application',
    'DoctrineModule',
    'DoctrineORMModule',
    //USER
    'ZfcUser',
    //RENDERER
    'ZendTwig',
    //MODULES
    'Betting',
];
