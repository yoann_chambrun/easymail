<?php

namespace Application\Entity;

class User extends \ZfcUser\Entity\User{

    private $role;

    public function setRole(String $role){
        $this->role = $role;
        return $this;
    }
    public function getRole(){
        return $this->role;
    }

    public function getAvatar(){
        return 'http://www.web-soluces.net/webmaster/avatar/Bitmoji-Fille.png';
    }

}