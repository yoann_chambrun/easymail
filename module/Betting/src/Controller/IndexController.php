<?php

namespace Betting\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use ZendTwig\View\TwigModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new TwigModel();
    }
}
